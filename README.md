# pyimport-sort-cdm

A Haskel learner's practice. It's only for personal usage. Don't install the
vim plugin to your environment.

<img src="example.gif" alt="example" width="75%"/>

## Policy for sorting python import statements

The policy is ruled by my colleague. It's very useful and readable for human.

- Mix `from` and `import` statements
- Sort the package name with case-insensitive
- Sort the names in `from import` with case-insensitive

## Install vim plugin

- vim-plug (Please install [stack](https://docs.haskellstack.org/en/stable/README/) first)

```vim
if executable('stack')
  Plug 'https://gitlab.com/yen3/pyimport-sort-cdm.git', {'for': 'python', 'do': 'stack install'}
endif
```
