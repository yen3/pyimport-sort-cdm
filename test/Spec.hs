{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import           Test.Tasty
import           Test.Tasty.HUnit

import qualified Test.TestPyImportCDM  as TP
import qualified Test.TestPyImportCDM2 as TP2

main :: IO ()
main = defaultMain $ testGroup "Unit Tests"
    [
      TP.tests,
      TP2.tests
    ]
