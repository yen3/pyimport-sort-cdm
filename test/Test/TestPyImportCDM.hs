{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Test.TestPyImportCDM where

import           Data.Text         (Text, intercalate, unpack)
import qualified Data.Text         as T

import           NeatInterpolation (text)
import           Test.Tasty
import           Test.Tasty.HUnit

import           PyImportCDM

importSortTest :: String -> Text -> Text -> TestTree
importSortTest desc is os = testCase desc $ os @?= join (pyImportSortCDM (T.lines is))
  where join = intercalate "\n"

tests :: TestTree
tests = testGroup "PyImportCDM Tests" [
      importSortTest "Test the python import statements are sorted" t1 a1,
      importSortTest "Test there is no import statements" t2 a2,
      importSortTest "Test to decouple multiple import modules" t3 a3
    ]


t1 :: Text
t1 = [text|#!/usr/bin/env python3

from pathlib import (
    PosixPath,
    Path,
)

import os.path as osp
from os import walk as os_walk
import json

from lxml import etree, abc

global_variable = 123
|]

a1 :: Text
a1 = [text|#!/usr/bin/env python3

from pathlib import Path, PosixPath

import json
from os import walk as os_walk
import os.path as osp

from lxml import abc, etree

global_variable = 123
|]

t2 :: Text
t2 = [text|global_variable = 1

def fun():
    pass
|]

a2 :: Text
a2 = t2

t3 :: Text
t3 = [text|#!/usr/bin/env python3

import c,d,a,b

global_variable = 123
|]

a3 :: Text
a3 = [text|
#!/usr/bin/env python3

import a
import b
import c
import d

global_variable = 123
|]
