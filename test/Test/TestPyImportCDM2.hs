{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Test.TestPyImportCDM2 where

import           Control.Applicative
import           Data.Text            (Text)
import qualified Data.Text            as T

import           Data.Attoparsec.Text
import           NeatInterpolation    (text)
import           Test.Tasty
import           Test.Tasty.HUnit

import           PyImportCDM2

data ParserTestParam a = ParserTestParam {
    ptDesc   :: Text,
    ptParser :: Parser a,
    ptInput  :: Text,
    ptAnswer :: a
  }

parserTC :: (Eq a, Show a) => ParserTestParam a -> TestTree
parserTC p = testCase (T.unpack . ptDesc $ p)
  (parseOnly (ptParser p) (ptInput p) @?= Right (ptAnswer p))

parserBasicTests :: TestTree
parserBasicTests = testGroup "Parser Basic Tests" $
  fmap parserTC
    [
      ParserTestParam "value seperated by comma"
        varSepParser " a, b, c, d" ["a", "b", "c", "d"]
    ]
  ++ fmap parserTC
    [
      ParserTestParam "simple import stat"
        simpleImportParser "import test" (Import "test"),
      ParserTestParam "simple import as stat"
        importAsParser "import a as b" (ImportAs "a" "b"),
      ParserTestParam "simple import as stat 2"
        (importAsParser <|> simpleImportParser) "import a as b"
        (ImportAs "a" "b"),
      ParserTestParam "from import stat"
        fromImportParser "from a import b" (FromImport "a" ["b"]),
      ParserTestParam "from import stat 2"
        fromImportParser "from a import b,c,d"
        (FromImport "a" ["b", "c", "d"]),
      ParserTestParam "from import stat 3"
        fromImportParser parserT1 (FromImport "a" ["b", "c", "d"]),
      ParserTestParam "from import as stat"
        fromImportAsParser "from a import b as c" (FromImportAs "a" "b" "c"),
      ParserTestParam "from import as stat 2"
        (fromImportAsParser <|> fromImportParser) "from a import b as c"
        (FromImportAs "a" "b" "c")
    ]

parserTests :: TestTree
parserTests = testGroup "Parser Tests" $ fmap parserTC
  [
    ParserTestParam "basic testCase A2" importParser parserT2 parserA2,
    ParserTestParam "basic testCase A3" importParser parserT3 parserA3,
    ParserTestParam "basic testCase A4" importParser parserT4 parserA4
  ]


rawInputTests :: TestTree
rawInputTests = testGroup "Raw Input Tests"
  [
    newTestCase "Normal input" t1 (T.concat [a1, "\n"]),
    newTestCase "No import statements" t2 a2,
    newTestCase "Input with ()" t3 (T.concat [a3, "\n"])
  ]
    where newTestCase desc input output = testCase desc (pyImportSortCDM2 input @?= output)

-- testCase "Raw input test" (pyImportSortCDM2 t1 @?= a1)
tests :: TestTree
tests = testGroup "PyImportCDM2 Unit Tests" [
    parserBasicTests,
    parserTests,
    testCase "splitRawImports test" (splitRawImports spiT1 @?= spiA1),
    testCase "findImportIndex test t1" (findImportIndex (T.lines t1) @?= Just (2, 12)),
    testCase "findImportIndex test t2" (findImportIndex (T.lines t2) @?= Nothing),
    testCase "findImportIndex test t3" (findImportIndex (T.lines t3) @?= Just (2, 8)),
    rawInputTests
  ]

-- Test input/output ----------------------------------------------------------

parserT1 :: Text
parserT1 = [text|from a import (b  ,
c,
d
                               )|]

parserT2 :: Text
parserT2 = [text|import a
from a import (b,
c,
d,
)
from b import c
import d as e
from f import g as h
|]

parserA2 :: [Import]
parserA2 = [
    Import "a",
    FromImport "a" ["b", "c", "d"],
    FromImport "b" ["c"],
    ImportAs "d" "e",
    FromImportAs "f" "g" "h"
  ]


parserT3 :: Text
parserT3 = [text|import json
from os import walk as os_walk
import os.path as osp
|]

parserA3 :: [Import]
parserA3 = [
    Import "json",
    FromImportAs "os" "walk" "os_walk",
    ImportAs "os.path" "osp"
  ]

parserT4 :: Text
parserT4 = [text|
import e
from a import (a,
    b,
    c,
    d
    )

|]
parserA4 :: [Import]
parserA4 = [
    Import "e" ,
    FromImport "a" ["a", "b", "c", "d"]
  ]


-- Test splitRawImports -------------------------------------------------------

spiT1 :: [Text]
spiT1 = [
    "from pathlib import (",
    "    PosixPath,",
    "    Path,",
    ")",
    "",
    "import os.path as osp",
    "from os import walk as os_walk",
    "import json",
    "",
    "from lxml import etree, abc"
  ]

spiA1 :: [[Text]]
spiA1 = [
    [
      "from pathlib import (",
      "    PosixPath,",
      "    Path,",
      ")"
    ],
    [
      "import os.path as osp",
      "from os import walk as os_walk",
      "import json"
    ],
    [
      "from lxml import etree, abc"
    ]
  ]

-- Test raw input -------------------------------------------------------------
t1 :: Text
t1 = [text|#!/usr/bin/env python3

from pathlib import (
    PosixPath,
    Path,
)

import os.path as osp
from os import walk as os_walk
import json

from lxml import etree, abc

global_variable = 123
|]

a1 :: Text
a1 = [text|#!/usr/bin/env python3

from pathlib import Path, PosixPath

import json
from os import walk as os_walk
import os.path as osp

from lxml import abc, etree

global_variable = 123

|]

t2 :: Text
t2 = [text|global_variable = 1

def fun():
    pass
|]

a2 :: Text
a2 = t2

t3 :: Text
t3 = [text|#!/usr/bin/env python3

import e
from a import (a,
    b,
    c,
    d
    )

global_variable = 1
|]

a3 :: Text
a3 = [text|#!/usr/bin/env python3

from a import a, b, c, d
import e

global_variable = 1


|]
