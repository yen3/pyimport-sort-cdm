" pyimport_sort_cdm.vim
" Author: Yen3
" Requires: Vim Ver7.0+
" Version:  0.1
"
" Documentation:
"   This plugin sort python import statements. It's only for personal use.
"   Please don't install to your vim environment
"
"   The plugin file steals from black.vim.
"
" History:
"  0.1:
"    - initial version

if v:version < 700 || !has('python3')
    func! __PYIMPORT_SORT_CDM_MISSING()
        echo "The pyimport_sort_cdm.vim plugin requires vim7.0+ with Python 3.6 support."
    endfunc
    command! PyImportSortCDM :call __PYIMPORT_SORT_CDM_MISSING()
    finish
endif

if exists("g:load_pyimport_sort_cdm")
   finish
endif

let g:load_pyimport_sort_cdm= "py1.0"
python3 << EndPython3

from pathlib import Path
import subprocess
import tempfile
from typing import Optional

import vim

BIN_DIRS = (Path("~/.local/bin/").expanduser(), Path("~/usr/bin").expanduser())
PYIMPORT_SORT_CDM_BIN = "pyimport-sort-cdm"


def get_exec() -> Optional[Path]:
    for path in (bin_dir / PYIMPORT_SORT_CDM_BIN for bin_dir in BIN_DIRS):
        if path.is_file():
            return path

    return None


def write_current_buffer(new_buffer: str) -> None:
    current_buffer = vim.current.window.buffer
    cursors = []
    for i, tabpage in enumerate(vim.tabpages):
        if tabpage.valid:
            for j, window in enumerate(tabpage.windows):
                if window.valid and window.buffer == current_buffer:
                    cursors.append((i, j, window.cursor))

    vim.current.buffer[:] = new_buffer.split("\n")[:-1]
    for i, j, cursor in cursors:
        window = vim.tabpages[i].windows[j]
        try:
            window.cursor = cursor
        except vim.error:
            window.cursor = (len(window.buffer), 0)


def PyImportSortCDM() -> None:
    exec_path = get_exec()
    if not exec_path:
        print(f"No executable for {PYIMPORT_SORT_CDM_BIN}")
        return

    with tempfile.TemporaryFile() as tempio:
        buffer_str = '\n'.join(vim.current.buffer) + '\n'

        tempio.write(buffer_str.encode())
        tempio.seek(0)

        new_buffer = subprocess.check_output(
            [str(exec_path)], stdin=tempio).decode("utf8")

    write_current_buffer(new_buffer)

EndPython3

command! PyImportSortCDM :py3 PyImportSortCDM()
