{-# LANGUAGE OverloadedStrings #-}

module PyImportCDM2 where

import           Control.Applicative
import           Control.Error.Util   (hush)
import           Control.Monad        (void)
import           Data.Char            (isAlphaNum, toLower)
import           Data.Function        (on)
import qualified Data.List            as L
import           Data.Maybe           (isJust, isNothing)
import           Data.Text            (Text)
import qualified Data.Text            as T

import           Data.Attoparsec.Text hiding (take)
import           GHC.Exts             (sortWith)


type Line = Text
type RawImportLine = Text

data Import =   Import       { moduleName :: Text }
              | ImportAs     { moduleName :: Text
                             , asName     :: Text }
              | FromImport   { moduleName :: Text
                             , attrs      :: [Text] }
              | FromImportAs { moduleName :: Text
                             , attr       :: Text
                             , asName     :: Text }
  deriving (Eq, Show)

instance Ord Import where
  compare x y = compare (lowerStr (moduleName x)) (lowerStr (moduleName y))

lineLength :: Int
lineLength = 120

lowerStr :: Text -> Text
lowerStr = T.map toLower

toText :: Import -> Text
toText (Import x)           = T.intercalate " " ["import", x]
toText (ImportAs x y)       = T.intercalate " " ["import", x, "as", y]
toText (FromImportAs x y z) = T.intercalate " " ["from", x, "import", y, "as", z]
toText (FromImport x ys)    =
  let line = T.intercalate " " ["from", x, "import", T.intercalate ", " ys]
      multiline =
        T.intercalate "\n" $ [T.concat ["from ", x, " import ("]]
          ++ fmap (\y -> T.concat ["    ", y, ","]) ys
          ++ [")"]
   in if T.length line < lineLength then line else multiline

skipInlineSpace :: Parser ()
skipInlineSpace =  skipWhile (\c -> c == '\t' || c == ' ')

skipEndOfLine :: Parser ()
skipEndOfLine =  skipWhile (== '\n')

skipKeyword :: Text -> Parser ()
skipKeyword w = void $ skipInlineSpace *> string w <* skipInlineSpace

varParser :: Parser Text
varParser = T.pack <$> many (satisfy (\c -> c == '_' || c == '.' || isAlphaNum c))

varSepParser :: Parser [Text]
varSepParser = filter (not . T.null) <$> sepBy1
  (many space *> varParser <* many space) (char ',')

simpleImportParser :: Parser Import
simpleImportParser = Import <$>
  do
    skipKeyword "import"
    n <- varParser
    skipInlineSpace
    skipEndOfLine
    return n

importAsParser :: Parser Import
importAsParser =
  do
    skipKeyword "import"
    n <- varParser
    skipKeyword "as"
    a <- varParser
    skipInlineSpace
    skipEndOfLine
    return $ ImportAs n a

-- TODO: Implement it or fix it
-- importMultiParser :: Parser [Import]
-- importMultiParser = do
  -- skipKeyword "import"
  -- vs <- varSepParser
  -- skipInlineSpace
  -- skipEndOfLine
  -- return $ fmap Import vs

fromImportParser :: Parser Import
fromImportParser =
  do
    skipKeyword "from"
    m <- varParser
    skipKeyword "import"
    ns <- (char '(' *> varSepParser <* char ')') <|> varSepParser
    skipInlineSpace
    skipEndOfLine
    return $ FromImport m ns

fromImportAsParser :: Parser Import
fromImportAsParser =
  do
    skipKeyword "from"
    m <- varParser
    skipKeyword "import"
    n <- varParser
    skipKeyword "as"
    a <- varParser
    skipInlineSpace
    skipEndOfLine
    return $ FromImportAs m n a

importParser :: Parser [Import]
importParser = many (
    fromImportAsParser <|> importAsParser <|>
    fromImportParser <|> simpleImportParser
  )

parseImport :: [Line] -> Maybe [Import]
parseImport xs = hush $ parseOnly importParser (T.unlines xs)

sortImports :: [Import] -> [Import]
sortImports xs = sortFromImport <$> L.sort xs
  where sortFromImport (FromImport n ys) = FromImport n (sortWith lowerStr ys)
        sortFromImport y                 = y

splitRawImports :: [Line] -> [[Line]]
splitRawImports xs =
  let rawSplit [] = []
      rawSplit ys =
        let (zs, rest) = break T.null ys
         in if null rest then [zs] else  zs : rawSplit (tail rest)
   in filter (not . null) (rawSplit xs)

sortImport :: [Line] -> Maybe [Line]
sortImport xs =
  fmap (T.unlines . fmap toText . sortImports) <$>
  traverse parseImport (splitRawImports xs)

findImportIndex :: [Text] -> Maybe (Int, Int)
findImportIndex xs =
  do
    bidx <- L.findIndex isImportPrefix xs
    re <- L.findIndex isImportPrefix (reverse xs)
    eidx <- let tidx = length xs - re
            in if isNothing (T.findIndex (== '(') (xs !! (tidx - 1)))
                 then Just tidx
                 else (tidx + 1 +) <$>
                   L.findIndex (isJust . T.findIndex (== ')')) (drop tidx xs)
    return (bidx, eidx)
  where
    isImportPrefix x = ((||) `on` flip T.isPrefixOf x) "from" "import"

pyImportSortCDM2 :: Text -> Text
pyImportSortCDM2 r =
  let rs = T.lines r
      importSort xs =
        do
          (b, e) <- findImportIndex xs
          ss <- sortImport (take (e - b + 1) $ drop b xs)
          return $ take b xs ++ ss ++ drop (e + 1) xs
   in
     maybe r T.unlines (importSort rs)
