{-# LANGUAGE OverloadedStrings #-}

module PyImportCDM where

import           Data.Char     (toLower)
import           Data.Function (on)
import           Data.List     (sort)
import           Data.Text     (Text)
import qualified Data.Text     as T
import           GHC.Exts      (sortWith)

type Line = Text
type RawImportLine = Text

data ImportStmt = Import { moduleName :: Text }
                | ImportAs { moduleName :: Text
                           , asName     :: Text }
                | FromImport { moduleName :: Text
                             , attrs      :: [Text] }
                | FromImportAs { moduleName :: Text
                               , attr       :: Text
                               , asName     :: Text }
  deriving Eq

instance Ord ImportStmt where
  compare x y = compare (lowerStr (moduleName x)) (lowerStr (moduleName y))

instance Show ImportStmt where
  show = T.unpack . importStmtText

importStmtText :: ImportStmt -> Text
importStmtText (Import x)           = T.intercalate " " ["import", x]
importStmtText (ImportAs x y)       = T.intercalate " " ["import", x, "as", y]
importStmtText (FromImport x ys)    = T.intercalate " " ["from", x, "import", T.intercalate ", " ys]
importStmtText (FromImportAs x y z) = T.intercalate " " ["from", x, "import", y, "as", z]

lowerStr :: Text -> Text
lowerStr = T.map toLower

isImportPrefix :: Line -> Bool
isImportPrefix x = ((||) `on` flip T.isPrefixOf x) "from" "import"

lineToStmts :: [Line] -> [RawImportLine]
lineToStmts [] = []
lineToStmts xs = T.concat [head xs, T.concat ys] : lineToStmts rest
  where (ys, rest)  = break isImportPrefix (tail xs)

toImportStmt :: RawImportLine -> [ImportStmt]
toImportStmt s =
    case head tokens of
      "import" -> if T.any (== ',') s
                     then fmap Import (drop 1 tokens)
                     else if length tokens == 4 && tokens !! 2 == "as"
                              then [ImportAs m (tokens !! 3)]
                              else [Import m]
      _ -> if length tokens == 6 && (tokens !! 4 == "as")
              then [FromImportAs m (tokens !! 3) (tokens !! 5)]
              else [FromImport m (drop 3 tokens)]
  where tokens = T.words $ T.map (\x -> if x == ',' || x == '(' || x == ')' then ' ' else x) s
        m = tokens !! 1


sortRawImports :: [Line] -> Text
sortRawImports = T.concat . fmap (T.pack . (++ "\n") . show) . sort .
    fmap sortFromImport . concatMap toImportStmt . lineToStmts
  where sortFromImport (FromImport n xs) = FromImport n (sortWith lowerStr xs)
        sortFromImport x                 = x

splitRawImports :: [Line] -> [[Line]]
splitRawImports [] = []
splitRawImports xs = let (ys, rest) = break T.null xs in ys : splitRawImports (tail rest)

pyImportSortCDM :: [Line] -> [Line]
pyImportSortCDM xs = maybe xs (importSort xs) (findIdx xs)
  where
    importSort xs (b, e) =
        take b xs
        ++ fmap sortRawImports (splitRawImports (take (e - b + 1) $ drop b xs))
        ++ drop (e + 1) xs
    findIdx xs =
      do
        bidx <- fstIdx isImportPrefix xs
        re <- fstIdx isImportPrefix (reverse xs)
        let eidx = length xs - re
        return (bidx, eidx)
    fstIdx f ys = let xs = filter (\(_, y) -> f y) (zip [0..] ys)
                   in if null xs then Nothing else Just (fst . head $ xs)
