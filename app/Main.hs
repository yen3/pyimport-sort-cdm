module Main where

import qualified Data.Text    as T
import           PyImportCDM2 (pyImportSortCDM2)

main :: IO ()
main = interact (T.unpack . pyImportSortCDM2 . T.pack)
